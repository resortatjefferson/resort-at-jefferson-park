Resort at Jefferson Park offers a setting for the active lifestyle within tranquil surroundings. We offer eight different floor plans, each one specially tailored with large, open rooms, expansive closets, and a choice of traditional or contemporary interiors. We have a home to fit your lifestyle.

Address: 1127 Hidden Ridge, Irving, TX 75038, USA
Phone: 972-435-7532
